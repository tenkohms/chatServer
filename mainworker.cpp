#include "mainworker.h"

#include <QDebug>

MainWorker::MainWorker(QObject *parent)
    : QObject(parent)
    , mSocketServer_( new QWebSocketServer( QStringLiteral( "Chat Server"),
                                            QWebSocketServer::NonSecureMode,
                                            this ) )
{
    if ( mSocketServer_->listen( QHostAddress::Any, 9090) )
    {
        qDebug() << "Listening on port: 8686";
        connect( mSocketServer_, SIGNAL( newConnection() ), this, SLOT( onNewConnection()) );
    }
}

MainWorker::~MainWorker() {
    mSocketServer_->close();
}

void MainWorker::onNewConnection()
{
    qDebug() << "New connection";
    auto pSocket = mSocketServer_->nextPendingConnection();
    //broadcastMessage( QString( pSocket->peerAddress().toString() + " connected!" ) );
    pSocket->setParent( this );

    connect( pSocket, SIGNAL(textMessageReceived(QString)), this, SLOT(broadcastMessage(QString)));
    connect( pSocket, SIGNAL(disconnected()), this, SLOT(socketDisconnected()));
    connect( pSocket, SIGNAL(binaryMessageReceived(QByteArray)), this, SLOT(onCommand(QByteArray)));
    mClients_.append( pSocket );
}

void MainWorker::broadcastMessage(const QString &message)
{
    qDebug() << "broadcasting message: " << message;
    for ( QWebSocket * client : qAsConst(mClients_) )
    {
        client->sendTextMessage( message );
    }
}

void MainWorker::socketDisconnected()
{
    QWebSocket * client = qobject_cast< QWebSocket *>(sender());
    if ( client )
    {
        QString username = usernames[ client ];
        mClients_.removeAll( client );
        client->deleteLater();

        broadcastMessage(QString( username + " left!") );
        usernames.remove( client );
        sendRoomList();
    }
}

void MainWorker::onCommand(const QByteArray &message)
{
    QWebSocket * client = qobject_cast< QWebSocket *>(sender());
    if ( client )
    {
        QString trans = QString::fromLocal8Bit( message );
        if ( trans.contains("username," ) )
        {
            QString newUser = trans.mid( trans.indexOf(",", 0) + 1, trans.size() - trans.indexOf(",", 0) );
            usernames[ client ] = newUser;
            broadcastMessage( QString( newUser + " joined!" ));
            sendRoomList();
        }
        if ( trans.contains("typing,") )
        {
            QString newUser = trans.mid( trans.indexOf(",", 0) + 1, trans.size() - trans.indexOf(",", 0) );
            for( QMap< QWebSocket *, QString>::iterator it = usernames.begin(); it != usernames.end(); it++ )
            {
                if ( newUser != it.value() )
                {
                    QWebSocket * client = it.key();
                    client->sendBinaryMessage( message );
                }
            }
        }
    }
}

void MainWorker::sendRoomList()
{
    QString roomList = "roomList,";
    for( QMap< QWebSocket *, QString>::iterator it = usernames.begin(); it != usernames.end(); it++ )
    {
        roomList.append( it.value() + ",");
    }

    roomList = roomList.mid(0, roomList.size() - 1 );
    for( QMap< QWebSocket *, QString>::iterator it = usernames.begin(); it != usernames.end(); it++ )
    {
        it.key()->sendBinaryMessage( roomList.toLocal8Bit() );
    }
}
