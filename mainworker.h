#ifndef MAINWORKER_H
#define MAINWORKER_H

#include <QObject>
#include <QWebSocket>
#include <QWebSocketServer>
#include <QMap>

class MainWorker : public QObject
{
    Q_OBJECT
public:
    explicit MainWorker(QObject *parent = nullptr);
    ~MainWorker();
signals:

private slots:
    void onNewConnection();
    void socketDisconnected();

    void broadcastMessage( const QString &message );
    void onCommand( const QByteArray &message );
    void sendRoomList();

private:
    QWebSocketServer * mSocketServer_;
    QList< QWebSocket * > mClients_;
    QMap< QWebSocket *, QString > usernames;
};

#endif // MAINWORKER_H
